FROM golang:1.20 as builder

ENV CGO_ENABLED=0
ENV GOARCH=amd64
ENV GOOS=linux

COPY . /src

RUN cd /src \
    && go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o k8sshields

FROM debian:bullseye

RUN set -x \
    && apt-get update -y \
    && apt-get install -y ca-certificates \
    && rm -rf /var/lib/apt/lists/* \
    && groupadd --gid 1664 gopher \
    && useradd --comment 'Go user' --uid 1664 --gid 1664 --shell /bin/sh gopher

COPY --from=builder /src/k8sshields /usr/local/bin/k8sshields

RUN chmod +x /usr/local/bin/k8sshields

USER gopher

ENTRYPOINT [ "/usr/local/bin/k8sshields" ]
