package main

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
)

var (
	shutdownInProgress    = false //nolint:gochecknoglobals
	wgDelta               = 2     //nolint:gochecknoglobals
	syscallSignalAddr     = 0xf   //nolint:gochecknoglobals
	factorTimeToSleep     = 15    //nolint:gochecknoglobals
	factorTimeoutDuration = 30    //nolint:gochecknoglobals
)

func main() { //nolint:funlen
	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/badges/kubernetes/namespace/{namespace}/{resource}/{ressourceName}", ResourceStatusController)

	rMetrics := mux.NewRouter()
	rMetrics.HandleFunc("/healthcheck", livenessHandler)
	rMetrics.HandleFunc("/healthz", livenessHandler)
	rMetrics.HandleFunc("/ready", readinessHandler)
	rMetrics.HandleFunc("/readiness", readinessHandler)
	rMetrics.Handle("/metrics", promhttp.Handler())

	timeoutDuration := time.Duration(factorTimeoutDuration) * time.Second
	srv := &http.Server{
		Addr:         "0.0.0.0:8080",
		Handler:      http.TimeoutHandler(router, timeoutDuration, "Server Timeout"),
		ReadTimeout:  timeoutDuration,
		WriteTimeout: timeoutDuration,
	}
	srvMetrics := &http.Server{
		Addr:         "0.0.0.0:9145",
		Handler:      http.TimeoutHandler(rMetrics, timeoutDuration, "Server Timeout"),
		ReadTimeout:  timeoutDuration,
		WriteTimeout: timeoutDuration,
	}

	var waitGroup sync.WaitGroup

	waitGroup.Add(wgDelta)
	// Run our server in a goroutine so that it doesn't block.
	go func() {
		defer waitGroup.Done()

		if err := srv.ListenAndServe(); err != nil {
			if errors.Is(err, http.ErrServerClosed) {
				log.Info("Server closed")
			} else {
				log.Fatalf("Failed to start server %v", err)
			}
		}
	}()

	go func() {
		defer waitGroup.Done()

		if err := srvMetrics.ListenAndServe(); err != nil {
			if errors.Is(err, http.ErrServerClosed) {
				log.Info("Metrics server closed")
			} else {
				log.Fatalf("Failed to start metrics server %v", err)
			}
		}
	}()

	// Graceful shutdown, inspired by https://github.com/gorilla/mux#graceful-shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.Signal(syscallSignalAddr)) // syscall.Signal(0xf) == SIGTERM
	<-c

	shutdownInProgress = true

	time.Sleep(time.Duration(factorTimeToSleep) * time.Second) // We give time to the readiness probe to be down
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(factorTimeToSleep))

	defer cancel()

	_ = srvMetrics.Shutdown(ctx)
	_ = srv.Shutdown(ctx)

	waitGroup.Wait()
	log.Info("Shutting down")
}
