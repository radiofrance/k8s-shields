package main

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/radiofrance/kubecli"
	appsv1 "k8s.io/api/apps/v1"
	errors2 "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

func ResourceStatusController(resp http.ResponseWriter, req *http.Request) { //nolint:funlen
	var (
		replicaSet        *appsv1.ReplicaSet
		statefulset       *appsv1.StatefulSet
		ressourceName     string
		readyPodsNumber   int32
		desiredPodsNumber int32
	)

	kube, err := kubecli.New("")
	if err != nil {
		log.Errorf("an error occurred during Kube Client setting,  %v", err)
		resp.WriteHeader(http.StatusInternalServerError)

		return
	}

	vars := mux.Vars(req)
	namespace := vars["namespace"]
	kube.Namespace = namespace
	resource := vars["resource"]

	switch {
	case resource == "deployment":
		ressourceName = vars["ressourceName"]
		replicaSet, err = kube.GetReplicaSetByName(ressourceName)
	case resource == "statefulset":
		ressourceName = vars["ressourceName"]
		statefulset, err = kube.GetStatefulSet(ressourceName)
	default:
		err = fmt.Errorf("not found: %w",
			errors2.NewNotFound(schema.GroupResource{Group: resource, Resource: "RessourceType"}, resource))
		log.Errorf("resource type not found,  %v", err)
		resp.WriteHeader(http.StatusNotFound)

		return
	}

	// error management
	if errors2.IsNotFound(errors.Unwrap(errors.Unwrap(err))) || errors2.IsNotFound(errors.Unwrap(err)) {
		log.Errorf("resource not found,  %v", err)
		resp.WriteHeader(http.StatusNotFound)

		return
	} else if err != nil {
		log.Errorf("an error occurred during resource retrieving,  %v", err)
		resp.WriteHeader(http.StatusInternalServerError)

		return
	}

	if replicaSet != nil {
		readyPodsNumber = replicaSet.Status.ReadyReplicas
		desiredPodsNumber = replicaSet.Status.Replicas
	} else {
		readyPodsNumber = statefulset.Status.ReadyReplicas
		desiredPodsNumber = statefulset.Status.Replicas
	}

	err = WriteAnswer(resp, namespace, ressourceName, readyPodsNumber, desiredPodsNumber)
	if err != nil {
		log.Errorf("an error occurred during answer writing step,  %+v\n", err)
		resp.WriteHeader(http.StatusInternalServerError)

		return
	}
}

func badgeBuilder(readyPodsNumber int32, desiredPodsNumber int32) string {
	var valueColor string

	for {
		switch {
		case readyPodsNumber == 0:
			valueColor = "red"
		case readyPodsNumber > 0 && readyPodsNumber < desiredPodsNumber:
			valueColor = "orange"
		case readyPodsNumber == desiredPodsNumber:
			valueColor = "green"
		default:
			valueColor = "red"
		}

		if readyPodsNumber <= desiredPodsNumber {
			break
		}
	}

	return valueColor
}

func WriteAnswer(resp http.ResponseWriter, namespace string, ressourceName string,
	readyPodsNumber int32, desiredPodsNumber int32,
) error {
	textLabel := namespace + "/" + ressourceName
	textValue := strconv.Itoa(int(readyPodsNumber)) + "/" + strconv.Itoa(int(desiredPodsNumber))

	valueColor := badgeBuilder(readyPodsNumber, desiredPodsNumber)

	tmpl, svgResult, err := svgBuild(textLabel, textValue, valueColor)
	if err != nil {
		return err
	}

	resp.Header().Set("Content-type", "image/svg+xml")
	resp.WriteHeader(http.StatusOK)

	if err := tmpl.Execute(resp, svgResult); err != nil {
		return fmt.Errorf("an error occurred during answer writing: %w", err)
	}

	return nil
}
