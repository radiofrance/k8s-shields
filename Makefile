## ----------------------
## Available make targets
## ----------------------
##

default: help

help: ## Display this message
	@grep -E '(^[a-zA-Z0-9_\-\.]+:.*?##.*$$)|(^##)' Makefile | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

clean: ## Clean tmp files
	rm -rf dist

##
## ----------------------
## Builds
## ----------------------
##

artifact: clean ## Generate binary in dist folder
	@CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o dist/k8sshields

##
## ----------------------
## Q.A
## ----------------------
##

qa: lint

lint.install: ## Install Go linter
	go install github.com/golangci/golangci-lint/cmd/golangci-lint

lint: ## Lint source code
	golangci-lint run -v

lint.fix: ## Lint and fix source code
	golangci-lint run --fix -v
