# k8s-shields

k8s-shields is an API returning a badge indicating the status of a deployment.
## How to use it ?

### How to quicky deploy it ?

To use it, you can apply directly  `recommended.yml` file like this :

`kubectl apply -f recommended.yml`

It will create a new namespace named `k8s-shields`, all resources necessary to test the project.

You should be able to reach the k8s-shields project from your browser using the Kube master node's public IP: 

`http://${kube_master_public_ip}:30080/badges/kubernetes/namespace/k8s-shields/deployment/k8s-shields`

### How to add it in your gitlab project ?

In your general project settings, go in badges section and add a badge.
You should define 3 fields :
   - the Name of the badge
   - the link to the page targeted by the added badge
   - the badge image URL with the following model:

`https://${domain}/badges/kubernetes/namespace/${namespace}/${resource}/${resourceName}`

The resource field could be [deployment](https://kubernetes.io/fr/docs/concepts/workloads/controllers/deployment/) or [statefulset](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/)

### Example :
 
   - Name : `DeploymentStatus`
   - Link : `https://gitlab.com/radiofrance/k8s-shields`
   - Badge image URL : `https:/${domain}/badges/kubernetes/namespace/prodtools/deployment/k8s-shields`
