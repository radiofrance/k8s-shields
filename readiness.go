package main

import (
	"net/http"

	log "github.com/sirupsen/logrus"
)

func livenessHandler(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-type", "application/json;charset=UTF-8")
	w.WriteHeader(http.StatusOK)
}

func readinessHandler(resp http.ResponseWriter, _ *http.Request) {
	if shutdownInProgress {
		log.Infof("Readiness failed, shutdown is in progress")
		resp.WriteHeader(http.StatusInternalServerError)

		return
	}

	resp.Header().Set("Content-type", "application/json;charset=UTF-8")
	resp.WriteHeader(http.StatusOK)
}
