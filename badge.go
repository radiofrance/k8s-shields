package main

import (
	"fmt"
	"path"
	"strconv"
	"strings"
	"text/template"
)

type BadgeParams struct {
	label                string
	value                string
	fontName             string
	fontSize             int
	numLabelPaddingChars float64
	numValuePaddingChars float64
	valuePrefix          string
	valueSuffix          string
	defaultColor         string
	valueFormat          string
	textColor            string
}

type SvgParams struct {
	BadgeWidth         string
	FontName           string
	FontSize           string
	Label              string
	ValueText          string
	LabelAnchor        string
	LabelAnchorShadow  string
	ValueAnchor        string
	ValueAnchorShadow  string
	BadgeColorCode     string
	LabelTextColor     string
	ValueTextColor     string
	ColorSplitPosition string
	ValueWidth         string
	MaskID             string
}

const (
	intEight     = 8
	intNine      = 9
	intTen       = 10
	intEleven    = 11
	floatFourty  = 40.0
	floatFifty   = 50.0
	floatSixty   = 60.0
	floatSeventy = 70.0
	floatHundred = 100.0
)

var FontWidths = map[string]map[int]int{ //nolint:gochecknoglobals
	"DejaVu Sans,Verdana,Geneva,sans-serif": {
		10: intNine,
		11: intTen,
		12: intEleven,
	},
	"Arial, Helvetica, sans-serif": {
		11: intEight,
	},
}

func WidthsCalcul(badgeParams BadgeParams, valueText string) (int, int) {
	fontWidth := fontWidthCalcul(badgeParams.fontName, badgeParams.fontSize)

	// The SVG width of the label text.
	labelWidth := int(float64(getTextWidth(badgeParams.label, fontWidth)) +
		(2.0 * badgeParams.numLabelPaddingChars * float64(fontWidth)))

	valueWidth := int(float64(getTextWidth(valueText, fontWidth)) +
		(2.0 * badgeParams.numValuePaddingChars * float64(fontWidth)))

	// The total width of badge
	badgeWidth := labelWidth + valueWidth

	return badgeWidth, valueWidth
}

func getTextWidth(text string, fontWidth int, fixedWidth ...bool) int {
	if len(fixedWidth) > 0 {
		return len(text) * fontWidth
	}

	size := 0.0

	// A dictionary containing percentages that relate to how wide
	// each character will be represented in a variable width font.
	// These percentages can be calculated using the ``_get_character_percentage_dict`` function.
	charWidthPercentages := map[string]float64{
		"lij|' ":                             floatFourty, //nolint:gocritic
		"![]fI.,:;/\\t":                      floatFifty,
		"`-(){}r\"":                          floatSixty,
		"*^zcsJkvxy":                         floatSeventy,
		"aebdhnopqug#$L+<>=?_~FZT0123456789": floatSeventy,
		"BSPEAKVXY&UwNRCHD":                  floatSeventy,
		"QGOMm%W@":                           floatHundred,
	}

	for _, char := range text {
		percentage := 100.0

		for k, v := range charWidthPercentages {
			if strings.Contains(k, string(char)) {
				percentage = v

				break
			}
		}

		size += (percentage / 100.0) * float64(fontWidth) //nolint:gomnd
	}

	return int(size)
}

func fontWidthCalcul(fontName string, fontSize int) int {
	return FontWidths[fontName][fontSize]
}

func ColorSplitPositionCalcul(badgeWidth int, valueWidth int) int {
	return badgeWidth - valueWidth
}

func AnchorsCalcul(badgeWidth int, colorSplitPosition int) (float64, float64) {
	divOperation := 2.0
	labelAnchor := float64(colorSplitPosition) / divOperation
	valueAnchor := float64(colorSplitPosition) + ((float64(badgeWidth) - float64(colorSplitPosition)) / divOperation)

	return labelAnchor, valueAnchor
}

func AnchorsShawdowCalcul(labelAnchor float64, valueAnchor float64) (float64, float64) {
	return labelAnchor + 1, valueAnchor + 1
}

func badgeColorCodeCalcul(defaultColor string) string {
	color := defaultColor
	Colors := map[string]string{
		"red":    "#e05d44",
		"green":  "#4c1",
		"orange": "#fe7d37",
	}

	if color[0] == '#' {
		return color
	}

	return Colors[color]
}

func svgParamsCalcul(badgeParams BadgeParams) SvgParams {
	valueText := badgeParams.valuePrefix + badgeParams.value + badgeParams.valueSuffix

	// textColor can be passed as a single value or a pair of comma delimited values
	textColors := strings.Split(badgeParams.textColor, ",")
	labelTextColor := textColors[0]
	valueTextColor := textColors[0]

	if len(textColors) > 1 {
		valueTextColor = textColors[1]
	}

	badgeWidth, valueWidth := WidthsCalcul(badgeParams, valueText)

	colorSplitPosition := ColorSplitPositionCalcul(badgeWidth, valueWidth)

	labelAnchor, valueAnchor := AnchorsCalcul(badgeWidth, colorSplitPosition)

	labelAnchorShadow, valueAnchorShadow := AnchorsShawdowCalcul(labelAnchor, valueAnchor)

	badgeColorCode := badgeColorCodeCalcul(badgeParams.defaultColor)

	fontName := badgeParams.fontName

	fontSize := badgeParams.fontSize

	label := badgeParams.label

	labelAnchorString := fmt.Sprintf("%f", labelAnchor)
	labelAnchorShadowString := fmt.Sprintf("%f", labelAnchorShadow)
	valueAnchorString := fmt.Sprintf("%f", valueAnchor)
	valueAnchorShadowString := fmt.Sprintf("%f", valueAnchorShadow)

	svgParams := SvgParams{
		strconv.Itoa(badgeWidth),
		fontName,
		strconv.Itoa(fontSize),
		label,
		valueText,
		labelAnchorString,
		labelAnchorShadowString,
		valueAnchorString,
		valueAnchorShadowString,
		badgeColorCode,
		labelTextColor,
		valueTextColor,
		strconv.Itoa(colorSplitPosition),
		strconv.Itoa(valueWidth),
		"0",
	}

	return svgParams
}

func svgBuild(label string, value string, valueColor string) (*template.Template, SvgParams, error) {
	fp := path.Join("/usr/local/bin/assets", "svgTemplate.xml")
	templateSvg, err := template.ParseFiles(fp)
	if err != nil {
		return nil, SvgParams{}, fmt.Errorf("an error occurred during SVG template Parsing: %w", err)
	}

	fontSizeValue := 11
	numPaddingCharsValue := 0.5
	badgeParams := BadgeParams{
		label:                label,
		value:                value,
		fontName:             "DejaVu Sans,Verdana,Geneva,sans-serif",
		fontSize:             fontSizeValue,
		numLabelPaddingChars: numPaddingCharsValue,
		numValuePaddingChars: numPaddingCharsValue,
		valueSuffix:          "",
		valuePrefix:          "",
		defaultColor:         valueColor,
		valueFormat:          "4",
		textColor:            "#fff",
	}

	svgParams := svgParamsCalcul(badgeParams)

	return templateSvg, svgParams, nil
}
